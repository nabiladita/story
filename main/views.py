from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'home.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def hobbies(request):
    return render(request, 'hobbies.html')

def experiences(request):
    return render(request, 'experiences.html')

def contact(request):
    return render(request, 'contact.html')


