from django.test import TestCase, Client
from .views import story8, fungsi_data
from django.urls import resolve


# Create your tests here.
class TestStory8(TestCase):
    
    def test_apakah_url_story8_ada(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code,200)

    def test_template_story8(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response,'story8.html')

    def test_func_story8(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)

    def test_apakah_ada_add_news_dan_tombol_add(self):
        response = Client().get('/story8/')
        template_name = response.content.decode('utf8')
        self.assertIn("Let's Find Book That You Want!",template_name)
        self.assertIn("Insert keyword",template_name)