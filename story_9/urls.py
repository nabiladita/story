from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
    path('',views.story9, name='story9'),
    path('signup/', views.signupPage, name='signup'),
	path('login/', views.loginPage, name='login'),  
	path('logout/', views.logoutPage, name='logout'),
]
