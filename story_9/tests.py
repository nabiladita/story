from django.test import TestCase, Client
from .views import story9,loginPage,signupPage,logoutPage
from django.urls import resolve
from .forms import CreateUserForm


# Create your tests here.
class TestStory9(TestCase):

    def test_apakah_url_login_ada(self):
        response = Client().get('/story9/login/')
        self.assertEquals(response.status_code,200)

    def test_apakah_url_signup_ada(self):
        response = Client().get('/story9/signup/')
        self.assertEquals(response.status_code,200)

    def test_template_signup(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response,'signup.html')

    def test_template_login(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response,'login.html')

    def test_func_story9(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)
    
    def test_func_loginPage(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, loginPage)

    def test_func_signupPage(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, signupPage)

    def test_apakah_ada_judul_tombol_signup(self):
        response = Client().get('/story9/signup/')
        template_name = response.content.decode('utf8')
        self.assertIn("Sign Up",template_name)
        self.assertIn("Username",template_name)
        self.assertIn("Email",template_name)
        self.assertIn("Password",template_name)
        self.assertIn("Confirm password",template_name)
        self.assertIn("Please register if you don't have any account",template_name)
        self.assertIn("Already have account?",template_name)
        self.assertIn("Login",template_name)

    def test_UserForm_valid(self):
        form = CreateUserForm(data={'username':"test", 'email': "test@gmail.com", 'password1': "satepadang", 'password2': "satepadang"})
        self.assertTrue(form.is_valid())

    def test_apakah_ada_judul_tombol_login(self):
        response = Client().get('/story9/login/')
        template_name = response.content.decode('utf8')
        self.assertIn("LOGIN",template_name)
        self.assertIn("Username",template_name)
        self.assertIn("Password",template_name)
        self.assertIn("Please login first to continue",template_name)
        self.assertIn("Don't have any account? Please register first",template_name)
        self.assertIn("Register",template_name)