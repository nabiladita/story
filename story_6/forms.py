from django import forms

from .models import Activities
from .models import Participants

class ActivitiesForm(forms.ModelForm):
    class Meta:
        model = Activities
        fields = [
            'Activity',
            'Description',
        ]

        widgets = {
            'Activity': forms.TextInput(attrs={'class': 'form-control',}),
            'Description': forms.Textarea(attrs={'class': 'form-control',}),
        }

class ParticipantsForm(forms.ModelForm):
    class Meta:
        model = Participants
        fields = [
            'Name',
        ]

        widgets = {
            'Name': forms.TextInput(attrs={'class': 'form-control',}),
        }
