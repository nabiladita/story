from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

from .forms import ActivitiesForm
from .forms import ParticipantsForm
from .models import Activities
from .models import Participants



def addparticipant(request,activity_id):
    participants = Participants.objects.all()
    activity = Activities.objects.get(id=activity_id)
    participants_form = ParticipantsForm(request.POST or None)

    if request.method == 'POST':
        if participants_form.is_valid():
            participants.create(
                Name = participants_form.data['Name'],
                activity = activity
            )
            return HttpResponseRedirect("/story6/")  

    context = {
        'page_title':'ADD PARTICIPANT',
        'participants_form': participants_form,
    }

    return render(request,'addparticipant.html',context)



def story6(request):
    participants = Participants.objects.all()
    activities = Activities.objects.all()
    context = {
        'page_title':'ACTIVITIES',
        'activities':activities,
        'participants':participants,
    }
    return render(request,'story6.html',context)


def addactivity(request):
    activities_form = ActivitiesForm(request.POST or None)

    if request.method == 'POST':
        if activities_form.is_valid():
            activities_form.save()

            return HttpResponseRedirect("/story6/")  

    context = {
        'page_title':'ADD ACTIVITY',
        'activities_form':activities_form,
    }

    return render(request,'addactivity.html',context)