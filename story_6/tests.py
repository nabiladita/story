from django.test import TestCase, Client
from .models import Activities
# Create your tests here.
class TestStory6(TestCase):
    
    def test_apakah_url_story6_ada(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code,200)

    def test_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response,'story6.html')

    def test_objek_dibuat(self) :
        Activities.objects.create(Activity='Belajar',Description='Di Perpusat')
        counter = Activities.objects.all().count()
        self.assertEqual(counter,1)
    

    