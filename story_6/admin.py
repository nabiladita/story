from django.contrib import admin

# Register your models here.

from .models import Activities
from .models import Participants

admin.site.register(Activities)
admin.site.register(Participants)