from django.urls import path
from . import views

app_name = 'story_6'

urlpatterns = [
    path('addparticipant/<int:activity_id>',views.addparticipant,name='addparticipant'),
    path('addactivity/',views.addactivity,name='addactivity'),
    path('',views.story6, name='story6'),
]