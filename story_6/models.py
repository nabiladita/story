from django.db import models

# Create your models here.

class Activities(models.Model):
    Activity = models.CharField(max_length=30)
    Description = models.TextField(max_length=70)


def __str__(self):
    return self.Activity

class Participants(models.Model):
    Name = models.CharField(max_length=30)
    activity = models.ForeignKey(Activities, on_delete=models.CASCADE)

    def __str__(self):
        return self.Name
