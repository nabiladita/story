"""story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story_8.views import fungsi_data

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('story5/', include('story_5.urls')),
    path('story1/', include('story_1.urls')),
    path('story6/', include('story_6.urls')),
    path('story7/', include('story_7.urls')),
    path('story8/', include('story_8.urls')),
    path('data/', fungsi_data),
    path('story9/', include('story_9.urls')),
]
