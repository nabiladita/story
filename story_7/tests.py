from django.test import TestCase, Client
from .views import story7
from django.urls import resolve


# Create your tests here.
class TestStory7(TestCase):
    
    def test_apakah_url_story7_ada(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code,200)

    def test_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response,'story7.html')

    def test_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7)
