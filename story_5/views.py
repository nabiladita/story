from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

from .forms import PostForm
from .models import Post
from django.views.generic.detail import DetailView

class PostDetailView(DetailView):
    model = Post
    template_name = "detail.html"

def delete(request,delete_id):
    Post.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/story5/")

# def detail(request,detail_id):
#     return render(request,"/detail/%s/"%(detail_id))

def story5(request):
    posts = Post.objects.all()
    context = {
        'page_title':'COURSES',
        'posts':posts,
    }
    return render(request,'story5.html',context)

def create(request):
    post_form = PostForm(request.POST or None)

    if request.method == 'POST':
        if post_form.is_valid():
            post_form.save()

            return HttpResponseRedirect("/story5/")  

    context = {
        'page_title':'ADD COURSE',
        'post_form':post_form,
    }

    return render(request,'create.html',context)