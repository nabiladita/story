from django import forms

# class PostForm(forms.Form):
#     nama = forms.CharField(max_length=20)
#     dosen = forms.CharField(max_length=20)
#     sks = forms.CharField(max_length=20)
#     deskripsi = forms.CharField(
#         widget = forms.Textarea
#         )
#     semester= forms.CharField(max_length=20)
#     ruangan= forms.CharField(max_length=20)

from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'Course',
            'Lecturer',
            'Credits',
            'Description',
            'Term',
            'Room',
        ]

        widgets = {
            'Course': forms.TextInput(attrs={'class': 'form-control',}),
            'Lecturer': forms.TextInput(attrs={'class': 'form-control',}),
            'Credits': forms.NumberInput(attrs={'class': 'form-control',}),
            'Description': forms.Textarea(attrs={'class': 'form-control',}),
            'Term': forms.TextInput(attrs={'class': 'form-control',}),
            'Room': forms.TextInput(attrs={'class': 'form-control',}),
        }
