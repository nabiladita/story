from django.db import models

# Create your models here.

class Post(models.Model):
    Course = models.CharField(max_length=30)
    Lecturer = models.CharField(max_length=30)
    Credits = models.PositiveIntegerField()
    Description = models.TextField(max_length=150)
    Term = models.CharField(max_length =20)
    Room = models.CharField(max_length =20)

    # published = models.DateTimeField(auto_now_add = True)
    # updated = models.DateTimeField(auto_now =True)

def __str__(self):
    return "{}. {}".format(self.id, self.Course)