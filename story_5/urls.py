from django.urls import path
from . import views
from .views import PostDetailView

app_name = 'story_5'

urlpatterns = [
    path('detail/<pk>/',PostDetailView.as_view(),name='detail'),
    path('delete/(?P<delete_id>[0-9]+)',views.delete,name='delete'),
    path('create/',views.create,name='create'),
    path('',views.story5, name='story5'),
]